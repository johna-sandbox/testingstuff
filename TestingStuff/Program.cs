﻿using System;
//Kenny's Change
namespace TestingStuff
{
    class Creds
    {
        string owner = "";
        string creds = "";

        public void SetUser(string owner, string creds)
        {
            this.owner = owner;
            this.creds = creds;
        }

        public void DisplayUser()
        {
            string credOutput = string.Format("Ownership: {0}\nCreds: {1}", owner, creds);
            Console.WriteLine(credOutput);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string[] usernames = { "jamieB", "johnA", "jeromeZ", "jasonL", "haleyS" };

            Creds[] credList = new Creds[usernames.Length];
            for (var i = 0; i < usernames.Length; i++)
            {
                credList[i] = new Creds();
                credList[i].SetUser(usernames[i], usernames[i] + "12345");
            }

            foreach (Creds x in credList)
            {
                Console.WriteLine("------");
                x.DisplayUser();
            }

            Console.WriteLine("Minor Chage");
            Console.WriteLine("Jeromes Change");
            Console.WriteLine("Jacobs Changes");
        }
    }
}
